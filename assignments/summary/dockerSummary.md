![](extras/doc.png)
# **What can I use Docker for?**
![](extras/doc1.PNG)<br>

- Fast, consistent delivery of your applications
- Responsive deployment and scaling
- Running more workloads on the same hardware

# **What is docker ?**
![](extras/doc2.png)<br>
Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.<br>
**The Docker Platform consists of multiple products/tools**
- Docker Engine
- Docker Hub
- Docker Trusted Registry
- Docker Machine
- Docker Compose
- Docker for Windows/Mac
- Docker Datacenter 
## **Docker architecture**

Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers.<br>
For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.![](extras/docart.PNG)


## **Docker Image**
![](extras/docimg.PNG)<br>
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization.

## **Docker Container**
A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. <br>
You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.
### **The Docker daemon**
The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services.
### **The Docker client**
The Docker client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.

### **Docker registries**
A Docker registry stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.

### **Docker objects**
When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.
## **Docker Hub**
![](extras/dochub.png)
Docker Hub is like GitHub for Docker Images. It is basically a cloud registry where you can find Docker Images uploaded by different communities, also you can develop your own image and upload on Docker Hub, but first, you need to create an account on DockerHub.

## **Docker Installation**
Docker is easy to install.<br>
It runs on:
- A variety of Linux distributions.
- OS X via a virtual machine.
- Microsoft Windows via a virtual machine.<br>

 **Install docker on ubuntu 16.04 LTS**<br>

 $ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"<br>
$ sudo apt-get update<br>
$ sudo apt-get install docker-ce docker-ce-cli containerd.io<br>

// Check if docker is successfully installed in your system <br>
$ sudo docker run hello-world 

## **Docker Basics Commands**
- docker run – Runs a command in a new container.
- docker start – Starts one or more stopped containers
- docker stop – Stops one or more running containers
- docker build – Builds an image form a Docker file
- docker pull – Pulls an image or a repository from a registry
- docker push – Pushes an image or a repository to a registry
- docker export – Exports a container’s filesystem as a tar archive
- docker exec – Runs a command in a run-time container
- docker search – Searches the Docker Hub for images
- docker attach – Attaches to a running container
- docker commit – Creates a new image from a container’s changes




