
![](extras/image1.png)

Git is a small yet very efficient version control tool. 
It helps both programmers and non-programmers keep 
track of the history of their project files by storing different versions of them.<br>
Git helps developers keep track of the history of their code
 files by storing them in different versions on its own server 
repository, i.e., GitHub. Git has all the functionality, performance,
 security, and flexibility that most of the development teams and 
individual developers need.

![](extras/image2.PNG)

## **Repository**
A repository, or Git project, encompasses the entire collection of files and folders associated with a project, along with each file’s revision history. <br>
Working in repositories keeps development projects organized and protected.<br>
Through platforms like GitHub and Gitlab, Git also provides more opportunities for project transparency and collaboration. Public repositories help teams work together to build the best possible final product.

![](extras/image3.PNG)

## **Git Life cycle**
![](extras/image4.PNG)

![](extras/image5.PNG)

## **Some Common Git Commands**

![](extras/image6.PNG)<br>
**Basic Git commands**<br>
To use Git, developers use specific commands to copy, create, change, and combine code. These commands can be executed directly from the command line or by using an application like GitHub Desktop or Git Kraken. Here are some common commands for using Git:

**git init** initializes a brand new Git repository and begins tracking an existing directory. It adds a hidden subfolder within the existing directory that houses the internal data structure required for version control.

**git clone** creates a local copy of a project that already exists remotely. The clone includes all the project’s files, history, and branches.

**git add** stages a change. Git tracks changes to a developer’s codebase, but it’s necessary to stage and take a snapshot of the changes to include them in the project’s history. This command performs staging, the first part of that two-step process. Any changes that are staged will become a part of the next snapshot and a part of the project’s history. Staging and committing separately gives developers complete control over the history of their project without changing how they code and work.

**git commit** saves the snapshot to the project history and completes the change-tracking process. In short, a commit functions like taking a photo. Anything that’s been staged with git add will become a part of the snapshot with git commit.

**git status** shows the status of changes as untracked, modified, or staged.

**git branch** shows the branches being worked on locally.

**git merge** merges lines of development together. This command is typically used to combine changes made on two distinct branches. For example, a developer would merge when they want to combine changes from a feature branch into the main branch for deployment.

**git pull** updates the local line of development with updates from its remote counterpart. Developers use this command if a teammate has made commits to a branch on a remote, and they would like to reflect those changes in their local environment.

**git push** updates the remote repository with any commits made locally to a branch.
## **Why Git version Control?**

![](extras/image7.PNG)<br>
**Git Integrity**<br>
Git is developed to ensure the security and integrity of content being version controlled. It uses checksum during transit or tampering with the file system to confirm that information is not lost. Internally it creates a checksum value from the contents of the file and then verifies it when transmitting or storing data.<br>
**Trendy Version Control System**<br>
Git is the most widely used version control system. It has maximum projects among all the version control systems. Due to its amazing workflow and features, it is a preferred choice of developers.<br>
**Everything is Local**<br>
Almost All operations of Git can be performed locally; this is a significant reason for the use of Git. We will not have to ensure internet connectivity.<br>
**Collaborate to Public Projects**<br>
There are many public projects available on the GitHub. We can collaborate on those projects and show our creativity to the world. Many developers are collaborating on public projects. The collaboration allows us to stand with experienced developers and learn a lot from them; thus, it takes our programming skills to the next level.<br>
**Impress Recruiters**<br>
We can impress recruiters by mentioning the Git and GitHub on our resume. Send your GitHub profile link to the HR of the organization you want to join. Show your skills and influence them through your work. It increases the chances of getting hired.

## **Git workflows**

![](extras/image8.PNG)

### **Centralized workflow**

![](extras/image9.PNG)<br>
The Centralized Workflow is a great Git workflow for teams transitioning from SVN. Like Subversion, the Centralized Workflow uses a central repository to serve as the single point-of-entry for all changes to the project. Instead of trunk, the default development branch is called master and all changes are committed into this branch. This workflow doesn’t require any other branches besides master.

### **Feature branching workflow**

![](extras/image10.PNG)<br>
Feature Branching is a logical extension of Centralized Workflow. The core idea behind the Feature Branch Workflow is that all feature development should take place in a dedicated branch instead of the master branch. This encapsulation makes it easy for multiple developers to work on a particular feature without disturbing the main codebase. It also means the master branch should never contain broken code, which is a huge advantage for continuous integration environments. 

### **Forking workflow**

![](extras/image11.PNG)<br>
The Forking Workflow is fundamentally different than the other workflows discussed in this tutorial. Instead of using a single server-side repository to act as the “central” codebase, it gives every developer a server-side repository. This means that each contributor has not one, but two Git repositories: a private local one and a public server-side one. 














